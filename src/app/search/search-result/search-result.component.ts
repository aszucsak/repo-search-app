import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { ResultData } from '../utilities/interfaces';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent {
  @Input() res: Observable<ResultData>;
}
