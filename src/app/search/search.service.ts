import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SEARCH_URL } from './utilities/constants';
import { switchMap, map, toArray } from 'rxjs/operators';
import { of } from 'rxjs';
import { QueryData, ResultData } from './utilities/interfaces';



@Injectable({
  providedIn: 'any'
})
export class SearchService {

  constructor(private http: HttpClient) { }

  launchSearch(query: QueryData) {
    return this.http.get(SEARCH_URL, { params: this.generateParams(query) }).pipe(
      switchMap((data: ResultData) => {
        const total_count = data.total_count;
        return of(...data.items).pipe(
          map(({
            name,
            full_name,
            description,
            forks,
            stargazers_count,
            watchers_count,
            issues,
            language,
            created_at,
            updated_at,
            owner
          }) => ({
            name,
            full_name,
            description,
            forks,
            stargazers_count,
            watchers_count,
            issues,
            language,
            created_at,
            updated_at,
            owner
          })),
          toArray(),
          map(items => ({ total_count, items }))
        )
      })
    );
  }

  private generateParams(query: QueryData): HttpParams {
    let qString = Object.entries(query).reduce((str, [key, value]) => {
      if (key === 'by' || value.length === 0) {
        return str;
      }

      if (['user', 'org'].includes(key)) {
        return `${str} ${key}:${value}`;
      }

      if (['in', 'language', 'topic'].includes(key)) {
        return `${str} ${key}:${value.join(',')}`;
      }

      if (['stars', 'size'].includes(key)) {
        return `${str} ${key}:${value.join('..')}`;
      }

      if (key === 'created') {
        const ISOValue = (value: Array<Date | null>) => {
          return value.map(v => v === null ? null : v.toISOString())
        }
        // Null value at index 2 means on or before/on or after
        if (value.length === 3) {
          const operator = value[0] === null ? '<=' : '>=';
          return `${str} ${key}:${operator}${ISOValue(value).join()}`;
        } else {
          return `${str} ${key}:${ISOValue(value).join('..')}`;
        }
      }
    }, query.by);

    const params = new HttpParams().set('q', qString).append('per_page', '10');

    return params;
  }

}
