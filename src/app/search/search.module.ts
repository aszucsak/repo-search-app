import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { SearchResultComponent } from './search-result/search-result.component';
import { SearchbarComponent } from './searchbar/searchbar.component';
import { SearchComponent } from './search.component';
import { SearchService } from './search.service';

const routes: Routes = [
  {
    path: '',
    component: SearchComponent
  }
]

@NgModule({
  declarations: [SearchResultComponent, SearchbarComponent, SearchComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
  ],
  providers: [SearchService]
})
export class SearchModule { }
