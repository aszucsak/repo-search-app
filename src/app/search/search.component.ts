import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { SearchService } from './search.service';
import { QueryData, ResultData } from './utilities/interfaces';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  searchResult: Observable<ResultData>;

  constructor(private searchService: SearchService) { }

  initSearch(queryData: QueryData): void {
    this.searchResult = this.searchService.launchSearch(queryData);
  }
}
