export const SEARCH_URL = 'https://api.github.com/search/repositories';
export const SEARCH_IN = ['name', 'description', 'readme'] as const;
