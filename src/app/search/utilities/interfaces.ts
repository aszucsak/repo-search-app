import { SEARCH_IN } from "./constants";

export interface Sort {
    sort: string;
    order: 'asc' | 'desc';
  }
  
  export interface Page {
    per_page: number;
    page?: number;
  }
  
  export type SearchIn = typeof SEARCH_IN[number];
  
  export interface QueryData {
    by: string;
    in: SearchIn[];
    user?: string;
    language?: string[];
    topic?: string[];
    org?: string;
    stars?: number[];
    size?: number[];
    created?: Date[];
  }
  
  export interface ResultRow {
    name: string;
    full_name: string;
    description: string;
    watchers_count: number;
    stargazers_count: number;
    forks: number;
    issues: number;
    created_at: string;
    updated_at: string;
    owner: Owner;
    language: string;
  }
  
  interface Owner {
    login: string;
    avatar_url: string;
  }
  
   export interface ResultData {
    total_count: number;
    items: ResultRow[];
  }