import { Component, EventEmitter, Input, Output } from '@angular/core';
import { QueryData, SearchIn } from '../utilities/interfaces';

@Component({
  selector: 'app-searchbar',
  templateUrl: './searchbar.component.html',
  styleUrls: ['./searchbar.component.scss']
})
export class SearchbarComponent {
  @Output() sendSearchTerm = new EventEmitter<QueryData>();
  
  searchText: string;

  getRepos($event: string): void {
    const queryData = {by: $event, in: ['name' as SearchIn]};
    this.sendSearchTerm.emit(queryData)
  }
}
